#include "ofApp.h"
/*
	By: Andrew Neel
	generative art with noise and balls

*/
//--------------------------------------------------------------
void ofApp::setup(){
	for (int x = 0; x < 10; x++) {
		for (int y = 0; y < 10; y++) {
			for (int z = 0; z < 10; z++) {
				int currOrb = ((x * 10) + y) * 10 + z; //calculate the index for the orb
				orbs[currOrb].setPosition(x * 20, y * 20, z * 20); //position the orb
				orbs[currOrb].setRadius(5); //set the size of the orbs
			}
		}
	}
	color.r = 240;
	color.g = 68;
	color.b = 32;
	cam.setPosition(100, 100, 300);

	light1.setDiffuseColor(ofColor(255, 250, 130));
	light1.setPosition(-100, 100, 500);
	light1.enable();

	light2.setDiffuseColor(ofColor(155, 150, 230));
	light2.setPosition(-400, -200, 00);
	light2.enable();
}

//--------------------------------------------------------------
void ofApp::update(){
	noiseTime += .05;

	for (int x = 0; x < 10; x++) {
		for (int y = 0; y < 10; y++) {
			for (int z = 0; z < 10; z++) {
				int currOrb = ((x * 10) + y) * 10 + z; //calculate the index again
				float myNoise = 25.0*ofSignedNoise(x / 8.0, y / 8.0 - noiseTime, z / 8.0, noiseTime); //figure out the noise at those coordinates
				float colorNoise = 80.0*ofSignedNoise(x / 18.0, y / 18.0 - noiseTime, z / 18.0, noiseTime);
				if (myNoise < 0)myNoise = 0; //do not use the negative noise
				orbs[currOrb].setRadius(myNoise); //change the size of the orb with the noise
				orbs[currOrb].setPosition(x * myNoise, y * myNoise, z * myNoise);//moves orbs based on noise
				// changes color based on noise
				if ((int)colorNoise % 3 == 0) {
					color.r = 240;
					color.g = 172;
					color.b = 32;
				}
				else if ((int)colorNoise % 5 == 0) {
					color.r = 204;
					color.g = 240;
					color.b = 32;
				}
				else if ((int)colorNoise % 2 == 0) {
					color.r = 240;
					color.g = 68;
					color.b = 32;
				}
			}
		}
	}
	
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofEnableDepthTest(); //sort the drawing so that the things closest to the camera is in front
	cam.begin();
	ofRotateDeg(45, 0, 1, 0); //rotate the cube 45 degrees on the y-axis

	for (int i = 0; i < 1000; i++) {
		ofSetColor(color);
		orbs[i].draw();//draw all the orbs
	}
	
	cam.end();
	ofDisableDepthTest();
	
	cam.draw(); //draw whatever the camera saw
}

