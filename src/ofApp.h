#pragma once

#include "ofMain.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		ofSpherePrimitive orbs[1000];
		ofSpherePrimitive o;
		ofEasyCam cam;
		ofLight light1, light2;
		float noiseTime;
		ofColor color;
};
